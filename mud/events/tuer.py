# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event3

class TuerAvecEvent(Event3):
    NAME = "tuer_avec"

    def perform(self):
        if not self.object.has_prop("life"):
            self.fail()
            return self.inform("tuez_avec.failed")
        self.inform("tuer_avec")
